export const environment = {
  production: true,
  url: 'https://dev.tuten.cl:443/TutenREST/rest/',
  app: 'APP_BCK',
  contactUser: 'contacto@tuten.cl',

};
