import {Injectable} from '@angular/core';
import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable()
export class JwtInterceptor implements HttpInterceptor {

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let authdata = JSON.parse(sessionStorage.getItem('login'));

    if (authdata) {
      request = request.clone({
        setHeaders: {
          'Authorization': `Bearer ${authdata.token}`,
          'Content-Type': 'application/json',
          'Cache-Control': 'no-cache, no-store, must-revalidate,  max-age=0',
          // 'Access-Control-Allow-Methods': 'GET, POST, DELETE, PUT',
          //'Access-Control-Allow-Origin': '*',
          //'X-Content-Type-Options': 'nosniff',
        }, setParams: {
          'current': 'true'
        }
      });
    }
    return next.handle(request);
  }
}
