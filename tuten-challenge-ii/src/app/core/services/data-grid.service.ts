import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpParams} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserSession} from '../model/user-session';
import {environment} from '../../../environments/environment';
import {TutenUser} from '../model/tuten-user';

@Injectable({
  providedIn: 'root'
})
export class DataGridService {

  url = environment.url;
  app = environment.app;
  contactUser = environment.contactUser;

  userSession: UserSession = JSON.parse(sessionStorage.getItem('login'));

  constructor(
    private httpClient: HttpClient
  ) {
  }

  getData(): Observable<TutenUser[]> {
    return this.httpClient.get<TutenUser[]>(this.url + 'user/' + this.contactUser + '/bookings', {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        app: this.app,
        adminemail: this.userSession.username,
        token: this.userSession.token,
        Accept: 'application/json',
      }), params: new HttpParams().set('current', 'true')
    });
  }

}
