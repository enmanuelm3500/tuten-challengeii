import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';
import {UserLogin} from '../model/user-login';
import {UserSession} from '../model/user-session';
import {environment} from '../../../environments/environment';
import {TutenUser} from '../model/tuten-user';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  url = environment.url;
  app = environment.app;


  userSession: UserSession;

  constructor(
    private httpClient: HttpClient
  ) {
  }

  authentication(data: UserLogin): Observable<TutenUser> {
    return this.httpClient.put<TutenUser>(this.url + 'user/' + data.email, null, {headers: new HttpHeaders({
        'Content-Type': 'application/json',
        app: this.app, password: data.password,
        Accept: 'application/json',

      })});
  }

  saveSession(dataAuth: UserSession) {
    sessionStorage.setItem('login', JSON.stringify(dataAuth));
  }

  cargarSession() {
    if (sessionStorage.getItem('login')) {
      this.userSession = JSON.parse(sessionStorage.getItem('login'));
    }
  }
}
