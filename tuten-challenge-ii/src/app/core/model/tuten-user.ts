export class TutenUser {
  serviceData: string;
  userAvailability: string;
  sessionTokenBck: string;
  firstName: string;
  email: string;
}
