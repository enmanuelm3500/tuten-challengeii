import {RouterModule, Routes} from '@angular/router';
import {PagesComponent} from './pages.component';
import {HomeComponent} from './home/home.component';
import {DataGridComponent} from './data-grid/data-grid.component';


const pagesRoutes: Routes = [
  {
    path: '',
    component: PagesComponent,
    children: [
      {path: 'home', component: HomeComponent},
      {path: 'datagrid', component: DataGridComponent},

      {path: '', redirectTo: '/home', pathMatch: 'full'}
    ]
  },
];
export const PAGES_ROUTES = RouterModule.forChild(pagesRoutes);
