import {Component, OnInit} from '@angular/core';
import {DataGridService} from '../../core/services/data-grid.service';

@Component({
  selector: 'app-data-grid',
  templateUrl: './data-grid.component.html',
  styleUrls: ['./data-grid.component.css']
})
export class DataGridComponent implements OnInit {

  data: any[] = [];
  filterPost: string = '';

  constructor(
    private dataservice: DataGridService
  ) {
  }

  ngOnInit(): void {
    this.dataservice.getData().subscribe(value => {
      console.log(value);
      this.data = value;
    });
  }


}
