import {ComponentFixture, TestBed} from '@angular/core/testing';

import {DataGridComponent} from './data-grid.component';
import {DataGridService} from '../../core/services/data-grid.service';
import {HttpClient} from '@angular/common/http';

describe('DataGridComponent', () => {
  let component: DataGridComponent;
  let fixture: ComponentFixture<DataGridComponent>;
  let comp;
  let service;

  beforeEach(() => {
    TestBed.configureTestingModule({
      // provide the component-under-test and dependent service
      providers: [
        DataGridComponent,
        { provide: DataGridService, useClass: MockUserService }
      ]
    });
    // inject both the component and the dependent service.
    comp = TestBed.inject(DataGridComponent);
    service = TestBed.inject(DataGridService);
  });

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [DataGridComponent]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(DataGridComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should welcome logged in user after Angular calls ngOnInit', () => {
    comp.ngOnInit();
  });
});

class MockUserService {
  isLoggedIn = true;
  user = { name: 'Test User'};
}
