import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';


@Component({
  selector: 'app-pages',
  templateUrl: './pages.component.html',
  styleUrls: ['./pages.component.css']
})
export class PagesComponent implements OnInit {

  admin = false;
  user: true;

  constructor(private router: Router) {
    if (!sessionStorage.getItem('login')) {
      this.router.navigate(['/login']);
    }
  }

  ngOnInit(): void {}


  logout() {
    sessionStorage.removeItem('login');
    this.router.navigate(['/login']);

  }

}
