import {CUSTOM_ELEMENTS_SCHEMA, NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SharedModule} from '../shared/shared.module';
import {PAGES_ROUTES} from './pages.routes';
import {PagesComponent} from './pages.component';
import { DataGridComponent } from './data-grid/data-grid.component';
import { FilterPipe } from './pipes/filter.pipe';


@NgModule({
  declarations: [
    PagesComponent,
    DataGridComponent,
    FilterPipe,
  ],
  exports: [
    FormsModule,
    ReactiveFormsModule,

  ],
  imports: [
    CommonModule,
    SharedModule,
    PAGES_ROUTES,
    FormsModule,
    ReactiveFormsModule,
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],

})
export class PagesModule {
}
