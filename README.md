             tuten front

Lenguaje de programación: TypeScript.
Framework: Angular CLI: 11.0.4. BootStrap 4.
Node: 14.15.1
Repositorio: git clone https://enmanuelm3500@bitbucket.org/enmanuelm3500/tuten-challengeii.git

Datos:
user: testapis@tuten.cl
pass: 1234

  Este challenge no fue desplegado en un servicio Cloud, debido a que el documento no lo pide. El paso a paso
para la implementacion en un servidor tomcat se encuentra en el documento adjunto, ubicado en la carpeta "implementacion"
una vez descargado el repositorio.

  Adicionalmente este challenge puede ser desplegado en una instancia de docker, siguiendo los siguientes pasos:
- Descargar el repositorio.
- Ingresar al proyecto desde una terminal, teclear los siguientes comandos. 
- npm install(debe tener instalado previamente npm)
- ng build --prod
- docker build -t tuten-challenge-ii .
- docker run -p 80:80 tuten-challenge-ii
- ingresar desde el navegador en la dirección http://localhost/#/login





